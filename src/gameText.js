export const introTextP1 =
  "Hi! Welcome to Lemonsville, California!\n"

export const introTextP2 =
  `In this small town, you are in charge of\n
  running your own lemonade stand. You can\n
  compete with as many other people as you\n
  wish, But how much profit you make is up\n
  to you (the other stands' sales will not\n
  affect your business in any way). If you\n
  make the most money, you're the winner!!`

export const introTextP3 =
  `Are you starting a new game? (yes or no)\n
  Type your answer and hit return ==>`

export const howManyPeopleText = 
"How many people will be playing?"

export const introText2 =
  `To manage your lemonade stand, you will\n
  need to make these decisions every day:\n
  \n
  1. How many glasses of lemonade to make\n
  (only one batch is made each morning)\n
  \n
  2. How many advertising signs to make\n
  (the signs cost fifteen cents each)\n
  \n
  3. What price to charge for each glass\n
  \n
  You will begin with $2.00 cash (assets).\n
  Because your mother gave you some sugar,
  yout cost to make lemonade is two cents\n
  a glass (this may change in the future).\n
  \n
  Press Space to continue, ESC to end...`

export const introText3 =
  `Your expenses are the sum of the cost of\n
  the lemonade and the cost of the signs.\n
  \n
  Your profits are the difference between\n
  the income from sales and your expenses.\n
  \n
  The number of glasses you sell each day\n
  depends on the price you charge, and on\n
  the number of advertising signs you use.\n
  \n
  Keep track of your assets, because you\n
  can't spend more money than you have!`