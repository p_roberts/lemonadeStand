import { writeText } from "./writeText"
import { gameText } from "./gameText"

let screen
const game = () => {
    return {
        start: () => setup()
    }
}
const setup = () => {
    screen = document.getElementById("text-paragraph")
    writeText(gameText.startText, screen)
    //screen.appendChild (<tspan>working</tspan>)

    // const spanArray = document.getElementsByTagName("tspan")
    // textSpan = spanArray[0]

    // writeText(gameText.startText, textSpan)

    // console.log(textSpan)
}

function GameUpdate() {

}

export default game
