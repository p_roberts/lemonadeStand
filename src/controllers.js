import { ConstantValues } from "./model"
import { model as gameState } from "./model"
import { InitialState } from "./model"

export function newGame() {
    gameState = InitialState
    playIntro()
    //StartNewDay(model)
    const today = new Day(gameState.day)
}

function playIntro() {
  
}

class Day {
    constructor (yesterday) {
        this.day = ++yesterday
        this.weather = MakeWeather(++yesterday)
        this.weatherText = `Weather Report for Day ${++yesterday}`
    }
}

class Globals {
    constructor () {
        this.state = {}
    }
    static getState() { return this.state }
}

export function StartNewDay(model) {
    model.day++
    model.weather = MakeWeather(model.day)
    model.weatherText = `Weather Report for Day ${model.day}`
    model.costText = `On day ${model.day}, the cost of lemonade is: `
    model = DoRandomEvents(model)
    if (model.randomEvents.specialEvent == false) {
        model.weatherText = model.weatherText + model.specialDescription
    }

    showDecisionPage()
}

function MakeWeather(day) {
    const rn = Math.random()
    if (rn < 0.6) {
        return ConstantValues.weatherSunny
    } else if (rn < 0.8) {
        return ConstantValues.weatherCloudy
    } else {
        if (day < 3) {
            return ConstantValues.weatherSunny
        } else {
            return ConstantValues.weatherHot
        }
    }
}

function DoRandomEvents(model) {
    model.randomEvents.streetCrewThirsty = false
    model.randomEvents.stormBrewing = false
    model.randomEvents.specialEvent = true
    const rn = Math.random()
    if (rn < 0.25 && model.weather == ConstantValues.weatherCloudy) {
        model.randomEvents.stormBrewing = true
    } else if (model.weather == ConstantValues.weatherSunny) {
        // HeatWave
        // Already handled in MakeWeather?
    } else {
        if (rn >= 0.25) {
            model.specialDescription = "No special event today"
            model.randomEvents.specialEvent = false
            return model
        }
        model.specialDescription = "The street department is working today.\nThere will be no traffic on your street."
        if (rn < 0.5) {
            model.randomEvents.streetCrewThirsty = true
        } else {
            model.randomEvents.weatherFactor = 0.1
        }
    }
    return model
}

const showDecisionPage = model => {
    let decisionObj = {}
    const cpgObj = costPerGlass(model.day)
    decisionObj.costPerGlass = cpgObj.cpGlass
    if (cpgObj.explanation !== undefined) {
        decisionObj.cpgExplanation = cpgObj.explanation
    } else {
        decisionObj.cpgExplanation =""
    }
    decisionObj.text = `$.0${costPerGlass}` // eg $.02
    decisionObj.assetsText = `$.0${model.assets}`
    decisionObj.howManyGlassesText = "How many glasses of lemonade "
}
const costPerGlass = day => {
    switch (day) {
    case day < 3:
        return {cpGlass: 2}
    case day === 3:
        return {
            cpGlass: 4,
            explanation: "Your mother quit giving you free sugar."
        }
    case day < 7:
        return {cpGlass: 4}
    case day === 7:
        return {
            cpGlass: 4,
            explanation: "The price of lemonade mix just went up."
        }
    default:
        alert(`there was an error in the 'costPerGlass' function. day value: ${day}, type: ${typeof day}`)
        break
    }
}