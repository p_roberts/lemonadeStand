let newText = ""
export const writeText = (text, target, index = 0) => {
    if (index == 0) {
        newText = ""
    }
    setTimeout(() => {
        newText += text[index]
        target.innerHTML = newText.toUpperCase()
        index++
        if (index < text.length) {
            writeText(text, target, index)
        }
    }, 30)
}