

export const ConstantValues = {
    startingAssets: 2.00,
    signCost: 0.15,
    newDayPage: 0,
    resultsPage: 1,
    p9: 10,
    s2: 30,
    c9: 0.5,
    c2: 1,
    weatherSunny: 2,
    weatherHot: 7,
    weatherCloudy: 10,
    weatherStorm: 5
}

export const InitialState = {
    day: 0,
    weather: 0,
    randomEvents: {
        streetCrewThirsty: false,
        stormBrewing: false
    }
}
export let gameState = {}