import intro from "./views/introPage"
import view from "./views"
import gamePhase from "./gamePhase"
window.onload = () => setTimeout(() => {

    const screen = new view()
    screen.getScreen("text-paragraph")
    screen.clear()
    const gp = gamePhase(screen)
    gp.next()
    setTimeout(() => {
        gp.next()
    }, 2000)
}, 1250)