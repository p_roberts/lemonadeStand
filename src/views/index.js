export default function screen () {
    return {
        screen: null,
        getScreen: (target) => {this.screen = document.getElementById(target)},
        addParagraph: (value) => {
            const p = document.createElement("p")
            const tn = document.createTextNode(value)
            p.appendChild(tn)
            this.screen.appendChild(p)
        },
        updateScreen: (value) => {
            if (typeof value === typeof "") {
                this.screen.innerHTML = value
            }
        },
        clear: () => { while (this.screen.hasChildNodes()) {
            this.screen.removeChild(this.screen.firstChild)
        }}
    }
}

// updateScreen(value, screen) {
//     if (typeof value === typeof "") {
//         screen.innerHTML = value
//     }
// }