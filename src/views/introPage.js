import {introTextP1} from "../gameText"
import {introTextP2} from "../gameText"
import {introTextP3} from "../gameText"

export default function (screen) {
    screen.addParagraph(introTextP1)
    screen.addParagraph(introTextP2)
    screen.addParagraph(introTextP3)
}